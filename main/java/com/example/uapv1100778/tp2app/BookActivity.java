package com.example.uapv1100778.tp2app;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class BookActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

        final Intent intent = getIntent();
        final Book book = intent.getParcelableExtra("book");

        final EditText nameBook = findViewById(R.id.nameBook);
        final EditText authorBook = findViewById(R.id.editAuthors);
        final EditText yearBook = findViewById(R.id.editYear);
        final EditText genreBook = findViewById(R.id.editGenres);
        final EditText publisherBook = findViewById(R.id.editPublisher);
        final Button saveButton = findViewById(R.id.button);

        if(book != null) {
            nameBook.setText(book.getTitle());
            authorBook.setText(book.getAuthors());
            yearBook.setText(book.getYear());
            genreBook.setText(book.getGenres());
            publisherBook.setText(book.getPublisher());
        }

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BookDbHelper bookDbHelper = new BookDbHelper(BookActivity.this);
                if(book != null){
                    book.setTitle(nameBook.getText().toString());
                    book.setAuthors(authorBook.getText().toString());
                    book.setYear(yearBook.getText().toString());
                    book.setGenres(genreBook.getText().toString());
                    book.setPublisher(publisherBook.getText().toString());
                    bookDbHelper.updateBook(book);
                }
                else {
                    Book newbook = new Book();
                    newbook.setTitle(nameBook.getText().toString());
                    newbook.setAuthors(authorBook.getText().toString());
                    newbook.setYear(yearBook.getText().toString());
                    newbook.setGenres(genreBook.getText().toString());
                    newbook.setPublisher(publisherBook.getText().toString());
                    bookDbHelper.addBook(newbook);
                }
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });

    }
}
