package com.example.uapv1100778.tp2app;

import android.content.Intent;
import android.database.Cursor;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.support.design.widget.FloatingActionButton;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;


import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    int UPDATE_BOOK_REQUEST_CODE = 1;
    int NEW_BOOK_REQUEST_CODE = 2;
    SimpleCursorAdapter adapter;
    BookDbHelper bookDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final FloatingActionButton saveButton = (FloatingActionButton) findViewById(R.id.addBookButton);
        final ListView listView = (ListView) findViewById(R.id.bookList);
        bookDbHelper = new BookDbHelper(this);


        Cursor result = bookDbHelper.fetchAllBooks();
        if(result == null){
            bookDbHelper.populate();
        }
        adapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                result,
                new String[] {BookDbHelper.COLUMN_BOOK_TITLE, BookDbHelper.COLUMN_AUTHORS},
                new int[] {android.R.id.text1, android.R.id.text2},
                0);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Cursor item = (Cursor) parent.getItemAtPosition(position);
                Intent intent = new Intent(MainActivity.this, BookActivity.class);
                intent.putExtra("book", BookDbHelper.cursorToBook(item));
                startActivityForResult(intent, UPDATE_BOOK_REQUEST_CODE);
            }
        });
        listView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                menu.add("Supprimer");
            }
        });
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, BookActivity.class);
                startActivityForResult(intent, NEW_BOOK_REQUEST_CODE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        adapter.changeCursor(bookDbHelper.fetchAllBooks());
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo menuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        View v = menuInfo.targetView;
        Cursor book = (Cursor) adapter.getItem(menuInfo.position);
        bookDbHelper.deleteBook(book);
        adapter.changeCursor(bookDbHelper.fetchAllBooks());
        adapter.notifyDataSetChanged();
        return true;
        //return super.onContextItemSelected(item);
    }
}
